<?php

require_once __DIR__ . '/vendor/autoload.php';

use Starbuzz\Contracts\StarbuzzStore;
use Starbuzz\Contracts\BeverageCreators\CoffeeCreator;
use Starbuzz\Contracts\BeverageCreators\TeaCreator;
use Starbuzz\Contracts\Collection;

$coffeeCreator = new CoffeeCreator();
$teaCreator = new TeaCreator();

$store = new StarbuzzStore(new Collection());
$store->setBeverageCreator($coffeeCreator);
$store->orderBeverage('espresso');
$store->orderBeverage('americano.milk');
$store->orderBeverage('americano.milk.whip');

$store->setBeverageCreator($teaCreator);
$store->orderBeverage('black.lemon');
$store->printCheck();
