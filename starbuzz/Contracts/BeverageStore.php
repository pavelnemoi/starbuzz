<?php

namespace StarBuzz\Contracts;

use Starbuzz\Beverages\Beverage;

interface BeverageStore
{
    public function orderBeverage($type);
    public function printCheck();
}