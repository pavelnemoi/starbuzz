<?php

namespace Starbuzz\Contracts;

use Starbuzz\Beverages\Beverage;
use Starbuzz\Contracts\BeverageStore;
use Starbuzz\Contracts\BeverageCreators\BeverageCreator;
use Starbuzz\Helpers\Logger;

class StarbuzzStore implements BeverageStore
{
    protected $beverageCreator;
    protected $beverage;
    protected $items;

    public function __construct(Collection $collection)
    {
        $this->items = $collection;
    }

    public function setBeverageCreator(BeverageCreator $creator)
    {
        $this->beverageCreator = $creator;
    }

    public function getBeverageCreator()
    {
        return $this->beverageCreator;
    }

    public function orderBeverage($type)
    {
        $this->beverage = $this->beverageCreator->create($type);
        $this->add($this->beverage);
        return $this->beverage->cost();
    }

    private function add(Beverage $beverage)
    {
        $this->items->append($beverage);
    }

    public function printCheck()
    {
        $it = $this->items->getIterator();
        while ($it->valid()) {
            $beverage = $it->current();
            Logger::get()->info($beverage->getDescription() . ' ' . $beverage->cost() . '$');
            $it->next();
        }
    }

    public function revenue()
    {
        $it = $this->items->getIterator();
        $sum = 0;
        while ($it->valid()) {
            $beverage = $it->current();
            $sum += $beverage->cost();
            $it->next();
        }
        return $sum;
    }
}