<?php

namespace Starbuzz\Contracts\Condiments;

use Starbuzz\Beverages\AbstractBeverage;
use Starbuzz\Beverages\Beverage;
use Starbuzz\Contracts\Price;

abstract class Condiment extends AbstractBeverage
{
    protected $beverage;
    const PRICE = 0;

    public function __construct(AbstractBeverage $beverage)
    {
        $this->beverage = $beverage;
    }

    protected function costBeverage()
    {
        return $this->beverage->cost();
    }

    public function getFullPrice()
    {
        return $this->costBeverage() + $this->getCondimentPrice();
    }

    public function getCondimentPrice()
    {
        return $this->getIntPrice(new Price(self::PRICE));
    }
}