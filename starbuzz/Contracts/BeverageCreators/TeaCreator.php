<?php

namespace Starbuzz\Contracts\BeverageCreators;

use Starbuzz\Contracts\BeverageCreators\BeverageCreator;

class TeaCreator extends BeverageCreator
{
    const TEA_NAMESPACE = 'Starbuzz\\Beverages\\Tea\\';

    public function create($type)
    {
        return $this->createClass(self::TEA_NAMESPACE, $type);
    }
}