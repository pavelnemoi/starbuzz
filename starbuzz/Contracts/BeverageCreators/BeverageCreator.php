<?php

namespace Starbuzz\Contracts\BeverageCreators;

use Starbuzz\Beverages\Coffee\Espresso;
use Starbuzz\Contracts\Price;

abstract class BeverageCreator
{
    public function createClass($namespace, $type)
    {
        return $this->createDependency($namespace, $type);
    }

    private function createDependency($namespace, $type)
    {
        $parts = explode('.', trim($type));
        $object = null;
        $className = $namespace;

        if (count($parts) == 1) {
            $className .= ucfirst($parts[0]);
            return new $className;
        }

        foreach ($parts as $part) {
            $className = $namespace .ucfirst($part);

            // add 'decorator string to class name'
            if ($object) {
                $className .= 'Decorator';
            }

            if (!class_exists($className)) {
                throw new \InvalidArgumentException('Class ' . $className . ' doesn`t exists');
            }

            if (!$object) {
                $object = new $className();
            } else {
                $object = new $className($object);
            }
        }

        return $object;
    }
}