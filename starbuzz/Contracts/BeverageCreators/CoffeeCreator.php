<?php

namespace Starbuzz\Contracts\BeverageCreators;

class CoffeeCreator extends BeverageCreator
{
    const COFFEE_NAMESPACE = 'Starbuzz\\Beverages\\Coffee\\';

    public function create($type)
    {
        return $this->createClass(self::COFFEE_NAMESPACE, $type);
    }
}