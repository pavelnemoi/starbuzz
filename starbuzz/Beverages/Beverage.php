<?php

namespace Starbuzz\Beverages;

interface Beverage
{
    public function getDescription();
    public function cost();
}