<?php

namespace Starbuzz\Beverages\Tea;

use Starbuzz\Contracts\Condiments\TeaCondiment;
use Starbuzz\Contracts\Price;

class LemonDecorator extends TeaCondiment
{
    public function getDescription()
    {
        return $this->beverage->getDescription() . ' with lemon';
    }

    public function cost()
    {
        return $this->getFullPrice(new Price(2));
    }
}