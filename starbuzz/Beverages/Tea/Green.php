<?php

namespace Starbuzz\Beverages\Tea;

use Starbuzz\Beverages\Tea\Tea;
use Starbuzz\Contracts\Price;

class GreenTea extends Tea
{
    public function getDescription()
    {
        return 'This is GreenTea';
    }

    public function cost()
    {
        return $this->getIntPrice(new Price(6));
    }
}