<?php

namespace Starbuzz\Beverages\Tea;

use Starbuzz\Beverages\Tea\Tea;
use Starbuzz\Contracts\Price;

class Black extends Tea
{
    public function getDescription()
    {
        return 'This is Black Tea';
    }

    public function cost()
    {
        return $this->getIntPrice(new Price(6));
    }
}