<?php

namespace Starbuzz\Beverages\Coffee;

use Starbuzz\Contracts\Condiments\CoffeeCondiment;
use Starbuzz\Contracts\Price;

class MilkDecorator extends CoffeeCondiment
{
    const PRICE = 5;

    public function getDescription()
    {
        return $this->beverage->getDescription() . ' with milk';
    }

    public function cost()
    {
        return $this->getFullPrice();
    }
}