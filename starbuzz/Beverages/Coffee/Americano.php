<?php

namespace Starbuzz\Beverages\Coffee;

use Starbuzz\Beverages\Coffee\Coffee;
use Starbuzz\Contracts\Price;

class Americano extends Coffee
{
    public function getDescription()
    {
        return 'This is Americano';
    }

    public function cost()
    {
        return $this->getIntPrice(new Price(8));
    }
}