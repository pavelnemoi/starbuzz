<?php

namespace Starbuzz\Beverages\Coffee;

use Starbuzz\Contracts\Condiments\CoffeeCondiment;
use Starbuzz\Contracts\Price;

class WhipDecorator extends CoffeeCondiment
{
    const PRICE = 1;

    public function getDescription()
    {
        return $this->beverage->getDescription() . ' with whip';
    }

    public function cost()
    {
        return $this->getFullPrice();
    }
}