<?php

namespace Starbuzz\Beverages\Coffee;

use Starbuzz\Beverages\Coffee\Coffee;
use Starbuzz\Contracts\Price;

class Espresso extends Coffee
{
    public function getDescription()
    {
        return 'This is Espresso';
    }

    public function cost()
    {
        return $this->getIntPrice(new Price(4));
    }
}