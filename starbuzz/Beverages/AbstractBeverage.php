<?php

namespace Starbuzz\Beverages;

use Starbuzz\Beverages\Beverage;
use Starbuzz\Contracts\Price;

abstract class AbstractBeverage implements Beverage
{
    public function getIntPrice(Price $p)
    {
        return $p->toNative();
    }
}