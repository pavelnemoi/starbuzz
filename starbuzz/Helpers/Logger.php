<?php

namespace Starbuzz\Helpers;

use Psr\Log\LoggerTrait;

final class Logger
{
    use LoggerTrait;

    private static $instanse;
    const PATH = './starbuzz.log';

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function get()
    {
        if (static::$instanse == null) {
            static::$instanse = new static;
        }

        return static::$instanse;
    }

    public function log($level, $message, array $context = array())
    {
        $message =  'Log level: ' . $level . ". Message: " . $message . "\n";
        file_put_contents(static::PATH, $message, FILE_APPEND | LOCK_EX);
    }
}