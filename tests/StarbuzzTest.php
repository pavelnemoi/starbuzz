<?php

use Starbuzz\Beverages\Coffee\MilkDecorator;
use Starbuzz\Beverages\Coffee\Americano;
use Starbuzz\Contracts\StarbuzzStore;
use Starbuzz\Contracts\BeverageCreators\CoffeeCreator;
use Starbuzz\Contracts\Collection;

class StarbuzzTest extends StarbuzzTestCase
{
    protected $store;

    public function setup()
    {
        parent::setup();

        $this->store = new StarbuzzStore(new Collection());
        $this->store->setBeverageCreator(new CoffeeCreator());
    }

    public function testBeverageCost()
    {
        $americano = new Americano();
        $price = $this->store->orderBeverage('americano');
        $this->assertEquals($price, $americano->cost());
    }

    public function testDecoratorCost()
    {
        $americano = $this->prophet->prophesize(Americano::class);
        $americano->cost()->willReturn(10);
        $milkDecorator = new MilkDecorator($americano->reveal());
        $this->assertEquals($milkDecorator->cost(), 10 + $milkDecorator->getCondimentPrice());
    }

    public function testBeverageCostWithDecorator()
    {
        $americano = new Americano();
        $milkDecorator = new MilkDecorator($americano);
        $price = $this->store->orderBeverage('americano.milk');
        $this->assertEquals($price, $milkDecorator->cost());
    }

    public function testAllCost()
    {
        $americano = new Americano();
        $milkDecorator = new MilkDecorator($americano);
        $this->store->orderBeverage('americano.milk');
        $this->store->orderBeverage('americano');
        $price = $americano->cost() + $milkDecorator->cost();
        $this->assertEquals($price, $this->store->revenue());
    }
}