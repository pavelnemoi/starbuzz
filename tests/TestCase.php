<?php

use Prophecy\Prophet;

abstract class StarbuzzTestCase extends \PHPUnit_Framework_TestCase
{
    protected $prophet;

    protected function setup()
    {
        $this->prophet = new Prophet;
    }

    protected function tearDown()
    {
        $this->prophet->checkPredictions();
    }
}